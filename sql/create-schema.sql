-- colon before variable: for a prepared statement using named placeholders, this will be a parameter name of the form :name

CREATE SCHEMA IF NOT EXISTS :schema;
SET search_path to :schema, public;

--CREATE
DROP TABLE IF EXISTS :schema.route_bundles;

CREATE TABLE IF NOT EXISTS :schema.route_bundles
(
id SMALLINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
long_name VARCHAR(64)
);

DROP TABLE IF EXISTS :schema.routes;

CREATE TABLE IF NOT EXISTS :schema.routes
(
id SMALLINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
short_name TEXT,
route_from TEXT,
route_to TEXT
);

DROP TABLE IF EXISTS :schema.service_levels;

CREATE TABLE IF NOT EXISTS :schema.service_levels
(
id SMALLINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
short_name VARCHAR(8),
long_name VARCHAR(64)
);

DROP TABLE IF EXISTS :schema.services;

CREATE TABLE IF NOT EXISTS :schema.services
(
id SMALLINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
route_id SMALLINT,
route_bundle_id SMALLINT,
service_level_id SMALLINT
);

INSERT INTO :schema.route_bundles(long_name) VALUES
('Bremen Bus'),
('Bremen Straßenbahn'),
('Bremerhaven'),
('Delmenhorst'),
('Oldenburg'),
('LK Ammerland Ost');
--TODO Adjust sequence counter after insert query!

INSERT INTO :schema.routes(short_name, route_from, route_to) VALUES
('1', 'Huchting', 'Bf Mahndort'),
('1S/E', 'Osterholz', 'Kirchbachstrasse'),
('2', 'Groepelingen', 'Sebalsbrueck'),
('26', 'Kattenturm', 'Ueberseestadt-Nord'),
('22', 'Kattenturm', 'Universitaet-Ost'),
('S', 'Leherheide', 'Bohmsiel'),
('502', 'Leherheide', 'Wulsdort'),
('Moon-Liner', 'Ziegeleistr.', 'Klinikum Bremerhaven'),
('515', 'Buschkaempen', 'Stadverwaltung'),
('330', 'Conneforde', 'Oldenburg'),
('370', 'Rastede', 'Bad Zwischenahn'),
('331', 'Conneforde', 'Wiefelstede'),
('347', 'Rastede', 'Rastede');
--TODO Adjust sequence counter after insert query!


--INSERT
INSERT INTO :schema.service_levels(short_name, long_name) VALUES
('1+', '1+'),
('1', '1'),
('2', '2'),
('3', '3'),
('SV', 'Stadtverkehr'),
('NV', 'Nachtverkehr'),
('BB', 'Buergerbus'),
('ALT', 'AnrufLinienTaxi');
--TODO Adjust sequence counter after insert query!
INSERT INTO :schema.services(route_id, route_bundle_id, service_level_id) VALUES
(1, 2, 5),
(2, 2, 5),
(3, 2, 5),
(4, 1, 5),
(5, 1, 5),
(6, 3, 5),
(7, 3, 5),
(8, 3, 6),
(9, 3, 8),
(10, 5, 1),
(11, 5, 3),
(12, 5, 4),
(13, 6, 7);
--TODO Adjust sequence counter after insert query!
