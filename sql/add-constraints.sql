-- colon before variable: for a prepared statement using named placeholders, this will be a parameter name of the form :name

CREATE SCHEMA IF NOT EXISTS :schema;
SET search_path to :schema, public;

ALTER TABLE :schema.route_bundles ALTER COLUMN long_name SET NOT NULL;

ALTER TABLE :schema.routes ALTER COLUMN short_name SET NOT NULL;
ALTER TABLE :schema.routes ALTER COLUMN route_from SET DEFAULT NULL;
ALTER TABLE :schema.routes ALTER COLUMN route_to SET DEFAULT NULL;

ALTER TABLE :schema.service_levels ALTER COLUMN short_name SET NOT NULL;
ALTER TABLE :schema.service_levels ALTER COLUMN long_name SET DEFAULT NULL;

ALTER TABLE :schema.services ADD FOREIGN KEY(route_id) REFERENCES :schema.routes(id);
ALTER TABLE :schema.services ADD FOREIGN KEY(route_bundle_id) REFERENCES :schema.route_bundles(id);
ALTER TABLE :schema.services ADD FOREIGN KEY(service_level_id) REFERENCES :schema.service_levels(id);
