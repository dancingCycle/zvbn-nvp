-- colon before variable: for a prepared statement using named placeholders, this will be a parameter name of the form :name
DROP SCHEMA IF EXISTS :schema CASCADE;
