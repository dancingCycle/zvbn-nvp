#!/bin/bash
#
echo "Started..."
#Started...
#
# special variable $# is the number of arguments
if [ $# -lt 3 ] ; then
    echo 'Call ./<script> <db name> <db user> <db schema>'
    exit 1
fi
#
DB_NAME="$1"
echo "DB_NAME: ${DB_NAME}"
DB_USER="$2"
echo "DB_USER: ${DB_USER}"
DB_SCHEMA="$3"
echo "DB_SCHEMA: ${DB_SCHEMA}"
#
psql -h localhost -p 5432 -U $DB_USER -f ./sql/add-constraints.sql -d $DB_NAME -v schema=$DB_SCHEMA
#
echo "Done."
#done.
